# Fleet meeting summary log

## Attendees
- Ziktofel
- Cherry
- Samuel
- Sofy
- Branden (came a bit late)
- Alandrii (a bit later)

## Promotions
- Ziktofel was promoted to admiral
- We'll use fleet uniforms for further promotions to Admiral
- Alandrii and Branden to Lt. Commanders

## Admada
- Armada leader still busy we expect a word from him next week
- We started the process to get rid of our gamma "-The Fallen-" as they're incative and under a minimum amount of members to create a fleet