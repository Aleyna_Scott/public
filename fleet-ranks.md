# Fleet Ranks

**Note: SUBJECT TO CHANGE**

## Fleet Admin 
The Fleet Admin acts as the head of the fleet and this rank exists for the purpose of ensuring our fleet is not hijacked (or as i like to call it Key holding) this rank also holds veto power over all aspects of the fleet and does not need permission to set fleet projects and can change any aspect of the fleet at anytime.

## Fleet Admiral 
Fleet Admiralty are well established officers that have a feel for our fleet they have permission to set events, call meetings, promote and demote members accordingly and look important.

## Admiral 
Admirals are our proven officers they have proven their loyalty to our fleet and are motivated in its day to day running and activities these officers usually have a task assigned to them by the Fleet Admin and are free to carry out the task (within reason) as they see fit they have permission to Host Events, request Prize Vault items for fleet events, promote and demote lower ranks according to our standards.

## Captain 
Captains are our entry level officers their job is to do the task assigned to them and create content for our members they have permission to host events and have access to all officer eyes only stuff.

## Commander 
Commanders are members who are trustworthy, active and social.

## LT CMDR 
These are players who are social and use fleet chat or join events or meetings

## LT
Entry level if you want to move up start chatting in fleet chat or join some events you need to show us that you want to blossom into a beautiful
